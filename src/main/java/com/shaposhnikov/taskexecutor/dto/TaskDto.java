package com.shaposhnikov.taskexecutor.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class TaskDto {
    private int id;
    private String name;
}
