package com.shaposhnikov.taskexecutor;

import com.shaposhnikov.taskexecutor.configuration.RabbitConfiguration;
import com.shaposhnikov.taskexecutor.service.workers.impl.RabbitMqTaskReportProducer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Scanner;

@SpringBootApplication
@EnableScheduling
public class TaskExecutorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskExecutorApplication.class, args);
	}


}
