package com.shaposhnikov.taskexecutor.shared;

import com.shaposhnikov.taskexecutor.model.AppInstance;
import com.shaposhnikov.taskexecutor.service.persistence.api.AppInstanceService;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class GlobalAppVariables {

    private final String exchangeName = "exchange-tasks";
    private final String consumerQueueName = "task-queue-";
    private final String producerQueueName = "task-reports";
    private final String suffix = " was finished.";
    private final AppInstanceService instanceService;
    private final AppInstance appInstance;

    public GlobalAppVariables(AppInstanceService instanceService) {
        this.instanceService = instanceService;
        this.appInstance = instanceService.addAppInstance();

    }

    public String getConsumerQueueName() {
        return consumerQueueName + appInstance.getId();
    }
}
