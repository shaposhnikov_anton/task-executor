package com.shaposhnikov.taskexecutor.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(of = {"id", "createdOn"})
@Entity
@Table(name = "app_instances")
public class AppInstance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    private LocalDateTime createdOn;
    @NonNull
    private LocalDateTime lastActivity;

}
