package com.shaposhnikov.taskexecutor.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(of = {"id", "name"})
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull
    private String name;
    @NonNull
    private int taskIdByClient;
    @NonNull
    private LocalDateTime executionDate;
    @NonNull
    @ManyToOne
    private AppInstance appInstance;
}
