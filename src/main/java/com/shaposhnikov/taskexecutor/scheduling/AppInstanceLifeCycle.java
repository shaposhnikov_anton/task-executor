package com.shaposhnikov.taskexecutor.scheduling;

import com.shaposhnikov.taskexecutor.model.AppInstance;
import com.shaposhnikov.taskexecutor.service.persistence.api.AppInstanceService;
import com.shaposhnikov.taskexecutor.shared.GlobalAppVariables;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class AppInstanceLifeCycle {

    private AppInstanceService service;
    private GlobalAppVariables appVariables;

    public AppInstanceLifeCycle(AppInstanceService service, GlobalAppVariables appVariables) {
        this.service = service;
        this.appVariables = appVariables;
    }

    @Scheduled(fixedRate = 120000, initialDelay = 120000)
    public void confirmInstanceActivity() {
        AppInstance instance = appVariables.getAppInstance();
        instance.setLastActivity(LocalDateTime.now());
        service.updateAppInstance(instance);
    }
}
