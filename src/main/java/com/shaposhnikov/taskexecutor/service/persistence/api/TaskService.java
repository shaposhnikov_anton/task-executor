package com.shaposhnikov.taskexecutor.service.persistence.api;

import com.shaposhnikov.taskexecutor.dto.TaskDto;

public interface TaskService {
    void addTask(TaskDto taskDto);
}
