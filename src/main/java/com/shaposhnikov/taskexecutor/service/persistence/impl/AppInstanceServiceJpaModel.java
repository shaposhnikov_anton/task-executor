package com.shaposhnikov.taskexecutor.service.persistence.impl;

import com.shaposhnikov.taskexecutor.dao.api.AppInstanceRepository;
import com.shaposhnikov.taskexecutor.model.AppInstance;
import com.shaposhnikov.taskexecutor.service.persistence.api.AppInstanceService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Profile("dev-jpa")
public class AppInstanceServiceJpaModel implements AppInstanceService {

    private AppInstanceRepository repository;

    public AppInstanceServiceJpaModel(AppInstanceRepository repository) {
        this.repository = repository;
    }

    @Override
    public AppInstance addAppInstance() {
        LocalDateTime now = LocalDateTime.now();
        AppInstance instance = new AppInstance(now, now);
        return repository.save(instance);
    }

    @Override
    public void updateAppInstance(AppInstance appInstance) {
        repository.save(appInstance);
    }
}
