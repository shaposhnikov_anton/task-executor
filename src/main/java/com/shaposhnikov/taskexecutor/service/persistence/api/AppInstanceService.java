package com.shaposhnikov.taskexecutor.service.persistence.api;

import com.shaposhnikov.taskexecutor.model.AppInstance;

public interface AppInstanceService {

    AppInstance addAppInstance();
    void updateAppInstance(AppInstance appInstance);

}
