package com.shaposhnikov.taskexecutor.service.persistence.impl;

import com.shaposhnikov.taskexecutor.dao.api.TaskRepository;
import com.shaposhnikov.taskexecutor.dto.TaskDto;
import com.shaposhnikov.taskexecutor.model.Task;
import com.shaposhnikov.taskexecutor.service.persistence.api.TaskService;
import com.shaposhnikov.taskexecutor.shared.GlobalAppVariables;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Profile("dev-jpa")
public class TaskServiceJpaModel implements TaskService {

    private TaskRepository repository;
    private GlobalAppVariables appVariables;

    public TaskServiceJpaModel(TaskRepository repository, GlobalAppVariables appVariables) {
        this.repository = repository;
        this.appVariables = appVariables;
    }

    @Override
    public void addTask(TaskDto taskDto) {
        Task task = new Task(taskDto.getName() + appVariables.getSuffix(), taskDto.getId(),
                LocalDateTime.now(), appVariables.getAppInstance());
        repository.save(task);
    }
}
