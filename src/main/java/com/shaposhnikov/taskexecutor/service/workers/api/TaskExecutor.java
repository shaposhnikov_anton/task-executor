package com.shaposhnikov.taskexecutor.service.workers.api;

import com.shaposhnikov.taskexecutor.dto.TaskDto;

public interface TaskExecutor {

    void processTask(byte[] msg);

}
