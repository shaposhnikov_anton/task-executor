package com.shaposhnikov.taskexecutor.service.workers.api;

public interface TaskReportProducer {

    void sendReport(String message);
    
}
