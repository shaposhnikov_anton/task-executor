package com.shaposhnikov.taskexecutor.service.workers.impl;

import com.shaposhnikov.taskexecutor.configuration.RabbitConfiguration;
import com.shaposhnikov.taskexecutor.service.workers.api.TaskReportProducer;
import com.shaposhnikov.taskexecutor.shared.GlobalAppVariables;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
public class RabbitMqTaskReportProducer implements TaskReportProducer {

    private final RabbitTemplate rabbitTemplate;
    private final GlobalAppVariables appVariables;

    public RabbitMqTaskReportProducer(RabbitTemplate rabbitTemplate, GlobalAppVariables appVariables) {
        this.rabbitTemplate = rabbitTemplate;
        this.appVariables = appVariables;
    }

    @Override
    public void sendReport(String message) {
        this.rabbitTemplate.convertAndSend(appVariables.getProducerQueueName(), message);
    }
}
