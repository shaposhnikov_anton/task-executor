package com.shaposhnikov.taskexecutor.service.workers.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shaposhnikov.taskexecutor.dto.TaskDto;
import com.shaposhnikov.taskexecutor.service.persistence.api.TaskService;
import com.shaposhnikov.taskexecutor.service.workers.api.TaskExecutor;
import com.shaposhnikov.taskexecutor.service.workers.api.TaskReportProducer;
import com.shaposhnikov.taskexecutor.shared.GlobalAppVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RabbitMqTaskExecutor implements TaskExecutor {

    private static final Logger logger = LoggerFactory.getLogger(RabbitMqTaskExecutor.class);
    private TaskService taskService;
    private TaskReportProducer reportProducer;

    public RabbitMqTaskExecutor(TaskService taskService, TaskReportProducer reportProducer) {
        this.taskService = taskService;
        this.reportProducer = reportProducer;
    }

    @Override
    @RabbitListener(queues = {"#{@globalAppVariables.getConsumerQueueName()}"})
    public void processTask(byte[] msg) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            TaskDto taskDto = mapper.readValue(msg, TaskDto.class);
            taskService.addTask(taskDto);
            reportProducer.sendReport(mapper.writeValueAsString(taskDto));
            logger.info(taskDto.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
