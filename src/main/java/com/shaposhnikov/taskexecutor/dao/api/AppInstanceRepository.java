package com.shaposhnikov.taskexecutor.dao.api;

import com.shaposhnikov.taskexecutor.model.AppInstance;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

@Profile("dev-jpa")
public interface AppInstanceRepository extends JpaRepository<AppInstance, Long> {
}
