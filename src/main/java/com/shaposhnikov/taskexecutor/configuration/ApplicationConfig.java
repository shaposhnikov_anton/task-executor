package com.shaposhnikov.taskexecutor.configuration;

import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

@Configuration
public class ApplicationConfig {

    @PreDestroy
    public void onShutDown() {
        new Thread() {
            @Override
            public void run() {
                System.out.println("Closing application context");
            }
        };
    }
}
