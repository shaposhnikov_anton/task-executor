package com.shaposhnikov.taskexecutor.configuration;

import com.shaposhnikov.taskexecutor.shared.GlobalAppVariables;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {

    private final GlobalAppVariables appVariables;

    public RabbitConfiguration(GlobalAppVariables appVariables) {
        this.appVariables = appVariables;
    }

    @Bean
    public Queue consumerQueue() {
        String queueName = appVariables.getConsumerQueueName();
        return new Queue(queueName, true, true, true);
    }

    @Bean
    public Queue producerQueue() {
        return new Queue(appVariables.getConsumerQueueName(), true, true, true);
    }

    @Bean
    public FanoutExchange exchange() {
        return new FanoutExchange(appVariables.getExchangeName());
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(consumerQueue()).to(exchange());
    }
}
