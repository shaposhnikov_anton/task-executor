This project is built with Maven using spring-boot-starter.

For connection to client side is used RabbitMQ server with two way binding. Server receives messages from client 
and replies with report about execution. For uninterrupted interaction with Rabbit server can be used any amount of instances. 
Every instance is listening ti its own queue that is created dynamically when instance start. All queues are binded to special exchange
to which client side app sends task messages. The task that comes to exchange is distributed to all queues ay the same time and every 
insctance is able to execute it and send report message back to client side.

For correct work RabbitMq server and Postgresql database should be run before starting instances.